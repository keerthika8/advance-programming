package assignment;

public class HouseHierechy {
	public static void main(String[] args) {

		RentedRoom[] arr = {new NormalRoom("NormalRoom", 5),new NormalRoom("NormalRoom", 10),new SemiLuxury("SemiLuxury", 2, 5),new SemiLuxury("SemiLuxury", 1, 6),new SuperLuxury("SuperLuxury", 101, 5),new SuperLuxury("SuperLuxury", 150, 1)};
		
		System.out.println(((NormalRoom)arr[0]).getName()+ "\tNumber of days : " +((NormalRoom)arr[0]).getnbDays() + "\t\tTotal Cost : " + ((NormalRoom)arr[0]).getCost()  );
		System.out.println(((NormalRoom)arr[1]).getName()+ "\tNumber of days : " +((NormalRoom)arr[1]).getnbDays() + "\t\tTotal Cost : " + ((NormalRoom)arr[1]).getCost()  );
		System.out.println(((SemiLuxury)arr[2]).getName()+ "\tNumber of beds : " +((SemiLuxury)arr[2]).getnbBeds() + "\t\tTotal Cost : " + ((SemiLuxury)arr[2]).getCost()  );
		System.out.println(((SemiLuxury)arr[3]).getName()+ "\tNumber of beds : " +((SemiLuxury)arr[3]).getnbBeds() + "\t\tTotal Cost : " + ((SemiLuxury)arr[3]).getCost()  );
		System.out.println(((SuperLuxury)arr[4]).getName()+ "\tNumber of squarefeet : " +((SuperLuxury)arr[4]).getsquareFeet() + "\tTotal Cost : " + ((SuperLuxury)arr[4]).getCost()  );
		System.out.println(((SuperLuxury)arr[5]).getName()+ "\tNumber of squarefeet : " +((SuperLuxury)arr[5]).getsquareFeet() + "\tTotal Cost : " + ((SuperLuxury)arr[5]).getCost()  );
 	}

}



class RentedRoom{
	private double baseFee;
	private String name;
	
	public RentedRoom(String name) {
		this.name=name;
		baseFee = 1000.00;
	}
	
	public double getCost() {
		return baseFee;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}




class LuxuryRoom extends RentedRoom{
	private int numberOfDays;
	private int rentalFees;
	

	public LuxuryRoom(String name, int numberOfDays) {
		super(name);
		this.numberOfDays=numberOfDays;
	}
	
	public double getRentalFees() {
		
		if (numberOfDays < 5 ){
			rentalFees = 100 * numberOfDays;
		}else if (5 <= numberOfDays && numberOfDays<= 10){
			rentalFees = 100 * numberOfDays;
		}else if(numberOfDays > 10) {
			rentalFees = 50 * numberOfDays;
		}
		return rentalFees;
		
	}
}




class SemiLuxury extends LuxuryRoom{
	private int nbBeds;
	
	public SemiLuxury(String name, int nbBeds, int numOfDays) {
		super(name,numOfDays);
		this.nbBeds=nbBeds;
	}
	
	public double getCost() {
		return nbBeds * super.getCost() * super.getRentalFees();
	}
	
	public int getnbBeds() {
		return nbBeds;
	}
}




class SuperLuxury extends LuxuryRoom{
	private double squareFeet;
	
	public SuperLuxury(String name, double squareFeet, int numOfDays) {
		super(name,numOfDays);
		this.squareFeet=squareFeet;
	}
	
	public double getCost() {
		return super.getCost()*squareFeet;
	}
	
	public double getsquareFeet() {
		return squareFeet;
	}
}




class NormalRoom extends RentedRoom{
	private int nbDays;
	
	public NormalRoom(String name,int nbDays) {
		super(name);
		this.nbDays=nbDays;
	}
	public double getCost() {
		return super.getCost()*nbDays;
	}
	
	public int getnbDays() {
		return nbDays;
}

}
